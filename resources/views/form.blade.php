<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>food Index</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
        crossorigin="anonymous"></script>
</head>

<body>

    <div class="container my-5">
        <div class="row">
            <div class="col-12 bg-dark text-white py-4">
                <form action="" id="taskForm">
                    <div class="form-group">
                        <select class="form-select" aria-label="Default select example" id="category">
                            @foreach ($categories as $row)
                                <option value="{{ $row->id }}">{{ $row->name }}</option>
                            @endforeach

                        </select>

                    </div>
                    <input type="text" id="name">
                    <button type="submit">submit</button>
                </form>
            </div>
        </div>
        <div class="row my-2">
            <div class="col-12">
                <div id="cards">
                    @foreach ($foods as $row)

                        <div class="card">
                            <div class="card-body">
                                {{ $row->id }} #
                                {{ $row->name }}
                                <button data-id={{ $row->id }} class="deleteBtn btn btn-danger">delete Me</button>
                            </div>

                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>



    <script>
        ///////////////////////////////////////////////////////////////////////////////
        // const
        const cardContainer = $('#cards');
        ///////////////////////////////////////////////////////////////////////////////
        // fucntions

        const updateUI = function(response) {
            $('#name').val('');
            const result = JSON.parse(response);
            //  console.log('the response is ' , $result);

            ///////////////////////////////////////////////////////////////////////////////
            // bad practice is to reload whole the page manually
            //window.location = window.location;
            ///////////////////////////////////////////////////////////////////////////////
            //Cleaning
            cardContainer.html(' ');

            $.each(result, function(key, value) {


                let div = `<div class="card">
                            <div class="card-body">
                                ${value.id} #
                                ${value.name}
                                <button data-id=${value.id} class="deleteBtn btn btn-danger">delete Me</button>
                            </div>

                        </div>`

                cardContainer.append(div)


            });


           const allNewElement =  document.querySelectorAll('.deleteBtn');

            allNewElement.forEach(function(element){

                    element.addEventListener('click', ()=>{ajaxDel(element.dataset.id)})
                   // element.addEventListener('click',()=>{ajaxDel(element.dataset.id)})
            })

        }






        const ajaxDel = function(id) {
            $.ajax({
                url: `/foodDelete/${id}/`,
                type: "GET",
                data: {},
                success: function(response) {
                    updateUI(response);
                }
            }); //Ajax done
        }


        ///////////////////////////////////////////////////////////////////////////////
        // jquery Event
        $('.deleteBtn').click(function() {
            console.log('clicked');
            let id = $(this).data('id');
            ajaxDel(id);
        })


        $('#taskForm').on('submit', function(e) {
            e.preventDefault();
            const name = $('#name').val();
            const category = $('#category').val();

            //Gaurd
            if (!name) {
                return;
            } else {
                $.ajax({
                    url: `/food/${name}/${category}`,
                    type: "GET",
                    data: {},
                    success: function(response) {
                            updateUI(response)

                    }
                });


            }

            console.log(name, category);


        });




    </script>

</body>

</html>
