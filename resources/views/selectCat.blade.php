<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
        crossorigin="anonymous"></script>
    <title>Document</title>
</head>

<body>
    <style>
        #resultCount {

            opacity: 0;
            transition: all 2s cubic-bezier(0.13, 0.95, 1, -0.12);
        }

        #resultCount.active {

            opacity: 1;
        }

    </style>
    <div class="container">
        <div class="row my-3 bg-dark text-white">
            <div class="col-12">
                <input type="text">
                <button id="searchBtn">search</button>
            </div>
            <div class="col-12 bg-info" id="answer">

            </div>
            <script>
                    let timer;

                $('input').keyup(function() {
                    console.log($(this).val());
                    let foodName = $(this).val();
                    let div = '';
                    let time = 5000;
                    console.log('timer before if is',timer);
                    if(timer) {

                        console.log('timer hast ',timer);

                    clearTimeout(timer)}

                    ;
                     timer = setTimeout(function() {
                        $.ajax({
                            url: "/ajax/searchFood/" + foodName,
                            type: "GET",
                            data: {},
                            success: function(response) {

                                const result = JSON.parse(response);
                                console.log('response is :', (result));

                                if (result) {
                                    $.each(result, function(index, value) {
                                        div += `food is ${value.name}<br/>`;
                                    });

                                } else {
                                    div = `nothing `;
                                }
                                $('#answer').html(div);
                            },
                        });




                    }, time);
                    timer;
                    console.log('timer is ',timer);





                })
            </script>







        </div>
        <div class="row">
            <div class="col-12">

                <h1>
                    change phase
                </h1>

                <h3 id="resultCount">

                </h3>

                <select class="form-select" aria-label="Default select example" id="category">
                    @foreach ($categories as $row)
                        <option value="{{ $row->id }}">{{ $row->name }}</option>
                    @endforeach

                </select>

                <hr>

                <select class="form-select" aria-label="Default select example" id="food">
                    <option value="">please choose your category first!</option>

                </select>

            </div>
        </div>
    </div>

    <script>
        $('#category').change(function() {
            let categoryId = $(this).val();
            console.log(categoryId);
            let foodSelector = $('#food');
            let resultCount = $('#resultCount');
            $.ajax({
                url: "/ajax/getFood/" + categoryId,
                type: "GET",
                data: {},
                success: function(response) {
                    //  foodSelector.html(response);
                    const result = JSON.parse(response);
                    console.log('response is :', (result));

                    resultCount.addClass('active');
                    resultCount.html('we found ' + result.length + ' foods for you');


                    foodSelector.html('');

                    $.each(result, function(index, value) {
                        let option = '<option value="' + value.id + '">' + value.name +
                            '</option>';
                        foodSelector.append(option)
                    })
                },
            });
        });
    </script>


    <script>
        // console.log('----- here h-----');

        // const factory =  {
        //         'name' : 'z4',
        //         'location':'mashhad',
        //         'time':{
        //             'shanbe':{
        //                 'open':10,
        //                 'close':16
        //             },
        //             'doshanbe':{
        //                 'open':11,
        //                 'close':14
        //             }
        //         }
        // }

        //  const factory2 =JSON.parse(JSON.stringify(factory));

        //  factory2.time.doshanbe.open = 8;
        //  console.log('factory 1 is ' ,factory);
        //  console.log('factory 2 is ' ,factory2);
        // let ghaza ={
        //                 name : 'abghoosh',
        //                 ashpaz : 'babak',
        //                 mavad : ['nokhod','goosht','ab','lobia']
        //             } ;
        //             console.log(ghaza);
        //         let a = [1 , 2];
        //         let b  = [3, 4];

        //         console.log(...name);


        // const student = [
        //             {
        //                 name:'babak',
        //                 age:'13'
        //             },
        //             {
        //                 name:'zahra',
        //                 age:'14'
        //             },
        //             {
        //                 name:'kianosh',
        //                 age:'10'
        //             }
        // ];
        // console.log(Object.entries(factory));
    </script>
</body>

</html>
