<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

        <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <style>
        .c1 {
            min-height: 100vh;
            background: lightblue;
        }

        .c2 {
            min-height: 100vh;
            background: lightgray;
        }

        .c3 {
            min-height: 100vh;
            background: lightslategray;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <ul class="nav nav-list">
                    <li>
                        <a href="{{route('change')}}"> change page</a>
                    </li>
                    <li>
                        <a href="{{route('foodIndex')}}"> Food Page</a>
                    </li> <li>
                        <a href="{{route('practice1')}}"> practice1 (splice,IndexOf)</a>
                    </li>
                     <li>
                        <a href="{{route('practice2')}}"> practice2(splice,findIndex)</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-3 c1">1</div>
            <div class="col-6 c2">
                <form id="taskForm"  >

                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">new task</label>
                        <input type="text" name="task" class="form-control" id="task">

                    </div>
                    <button>submit</button>
                </form>
                <hr>
                <div id="cards">
                @foreach ($data as $row )

                        <div class="card">
                            <div class="card-body">
                                {{$row->task}}
                            </div>
                        </div>
               @endforeach
               </div>

            </div>
            <div class="col-3 c3">3</div>
        </div>
    </div>
    <script>

//console.log('what is typeof the undifined ',typeof null);




        $('#taskForm').on('submit',function(e){
               e.preventDefault();
               let newtask = $('#task').val();

               $.ajax({
                   url:  '/task/'+newtask ,
                   type: "GET"  ,
                   data:{}   ,
                   success:function(response){

                        console.log('the response is ' , response);
                        const div = '<div class="card"><div class="card-body">'+response+'</div></div>';



                      //  $('#cards').append(div);
                          $('#cards').append(div);
                   }
               });



        });




      $('#taskForm').on('submit',function(e){
        e.preventDefault();

         let task = $('#task').val();

        $.ajax({
          url: "/task",
          type:"POST",
          data:{
            "_token": "{{ csrf_token() }}",
            task:task,
          },
          success:function(response){
        //    console.log('response is :',response);
          },
         });
        });
      </script>
    </script>
</body>

</html>
