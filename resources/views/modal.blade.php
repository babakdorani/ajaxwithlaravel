<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
            crossorigin="anonymous"
        />

        <script
            src="https://code.jquery.com/jquery-3.6.0.js"
            integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
            crossorigin="anonymous"
        ></script>
        <style>
            body {
                background:url('https://images.unsplash.com/photo-1469474968028-56623f02e42e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1174&q=80')  no-repeat center center fixed;
                background-size: cover;
            }
            .layout{
            position: fixed;
            width: 100%;
            height: 100%;

                filter: blur(200px);
            z-index: 1000;
            display: block;
            top: 0;
            left: 0;
            display:none;
            }
            .layout.active{
                display: block;
            }
            .notification{
                width: 60%;
    height: 500px;
    background: white;
    left: 0;
    right: 0;
    position: fixed;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    z-index: 1001;
    justify-self: center;
    align-self: center;
    margin: 0 auto;
    display:none;
            }
            .notification.active{
                display:block;
            }


        </style>
        <title>Document</title>
    </head>
    <body>
        <button class="btn btn-danger showModal">show me</button>




        <div class="layout">
        </div>
        <div class="notification">
            <div class="notification__header">
                <span>
                        title
                </span>
                <span class="closeModal">
                    X
                </span>
            </div>
            <div class="notification__content">
                    <p>
                        lorem ipsum dolor sit amet, consectetur adip
                    </p>
            </div>
         <div>
    </body>
    <script>
        ///////////////////////////////////////////////////////////////////////////////
        // const and deceleration
        const btn = document.querySelector(".showModal");
        const layout = document.querySelector(".layout");
        const notification = document.querySelector(".notification");
        const closeModal = document.querySelector(".closeModal");

        //audio
        const audio=document.createElement("audio");
        audio.setAttribute('src','/statics/iman.mp3');
        //////////////////////////////////////////////////////////////////////////////
        //functions
        const showModal = ()=>{

            //display layout first
            layout.classList.add('active');
            //display modal
            notification.classList.add('active');
            audio.play();
        }
        const closeModalFunc = ()=>{
            //display layout first
               layout.classList.remove('active');
            //display modal
            notification.classList.remove('active');
        }
        const pauseAudio = ()=>{
                audio.pause();
        }
///////////////////////////////////////////////////////////////////////////////
//Events
        closeModal.addEventListener('click',closeModalFunc);
        btn.addEventListener("click", showModal);
        layout.addEventListener("click",closeModalFunc);
        document.addEventListener('keydown', function (event) {
            console.log(event)
            if(event.key ==='F1'){
                audio.play();
            }if(event.key ==='F2'){
                audio.volume += 0.1;
            }if(event.key ==='F3'){
                audio.volume -= 0.1;
            }
            if(event.key === "Escape" ){
               audio.pause();
            }
        })

//         <button onclick="document.getElementById('demo').volume+=0.1">Increase Volume</button>
//   <button onclick="document.getElementById('demo').volume-=0.1">Decrease Volume</button>



        </script>
</html>
