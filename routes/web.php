<?php

use App\Http\Controllers\AjaxController;
use App\Http\Controllers\JsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/practice1',[JsController::class,'splicePractice1'])->name('practice1');

Route::get('/practice2',[JsController::class,'findIndexSplicePractice'])->name('practice2');

Route::get('/', [AjaxController::class,'index'])->name('home');
Route::get('/modal',[AjaxController::class,'modal'])->name('modal');
Route::get('/task/{newTask}',[AjaxController::class,'storeTask'])->name('storeTask');

Route::get('/change', [AjaxController::class,'change'])->name('change');



Route::post('/task',[AjaxController::class,'store'])->name('store');

Route::get('/ajax/getFood/{categoryId}',[AjaxController::class,'getFood']);

Route::get('/ajax/searchFood/{foodName}',[AjaxController::class,'searchFood']);

Route::get('/food',[AjaxController::class,'foodIndex'])->name('foodIndex');
Route::get('/food/{name}/{category}',[AjaxController::class,'foodSave'])->name('foodSave');
Route::get('/foodDelete/{id}/',[AjaxController::class,'foodDelete'])->name('foodDelete');
