<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Food;
use App\Models\task;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    //
    public function modal(){
        return view('modal');
    }
    public function index(){
            $data = task::orderBy('id','desc')->get();
            return view('welcome',compact('data'));

    }
    public function indexJson(){
            $data = task::orderBy('id','desc')->get();
            return var_dump(json_encode($data));

    }


    public function storeTask($newTask){
        $task = new task();
        $task->task = $newTask;
        $task->save();

       // $data= task::orderBy('id','desc')->get();

        return $newTask;
    }

    public function store(Request $request)

    {
     //   dd($value)
     return $request->input('task');
        task::create($request->all());
        return redirect()->back();
    }





    public function change(){
        $data['categories'] = Category::get();
        return view('selectCat',$data);
    }
    public function getFood($categoryId){

        $food = Food::where('category_id',$categoryId)->get();
//         $output='<option>choose something</option>';
//         foreach($food as $row){
//             $output .= '<option value="'.$row->id .'">'.$row->name.'</option>' ;
//         }

// return $output;
        return json_encode($food);

    }



    public function searchFood($foodName){

        $result = Food::where('name','LIKE' , '%'.$foodName.'%')->get();


        return json_encode($result);
    }




     public function foodIndex(){
        $data['categories'] = Category::get();
        $data['foods']=Food::orderBy('id','desc')->get();
        return view('form',$data);
     }

     public function foodSave($name,$category){

       $food = new Food();
       $food->name = $name;
       $food->category_id = $category;
       $food->save();
        $foods=Food::orderBy('id','desc')->get();
       return json_encode($foods);

     }


     public function foodDelete($id){

       Food::findOrFail($id)->delete();
         $foods=Food::orderBy('id','desc')->get();
        return json_encode($foods);

      }
}
