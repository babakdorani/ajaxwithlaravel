<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class JsController extends Controller
{
    //
    public function splicePractice1(){
        return view('spliceShiftPopPractice');
    }

    public function findIndexSplicePractice(){
        return view('spliceFilterPractice');
    }
}
