///////////////////////////////////////////////////////////////////////////////const and deceleration
const body = document.querySelector("body");
const maxTry = 3;
let yourTry = 0;
let index;

const rewards = [
  { number: 1, isReward: false, reward: "Maybe Next Time 💣" },
  { number: 2, isReward: false, reward: "Maybe Next Time 💣" },
  { number: 3, isReward: false, reward: "Maybe Next Time 💣" },
  { number: 4, isReward: true, reward: "you have won a bicycle" },
  { number: 5, isReward: false, reward: "Maybe Next Time 💣" },
  { number: 6, isReward: false, reward: "Maybe Next Time 💣" },
  { number: 7, isReward: true, reward: "you have won 1000$" },
  { number: 8, isReward: false, reward: "Maybe Next Time 💣" },
  { number: 9, isReward: false, reward: "Maybe Next Time 💣" },
  { number: 10, isReward: false, reward: "Maybe Next Time 💣" },
  { number: 11, isReward: false, reward: "Maybe Next Time 💣" },
  { number: 12, isReward: false, reward: "Maybe Next Time 💣" },
];

const playTheGame = function () {
  if (yourTry >= maxTry) {
    alert("game over");
    return;
  }
  body.style.color = "white";

  const answer = +prompt(
    `you have ${rewards.length} choices and you have just ${
      maxTry - yourTry
    } chances.write the number between 1 and ${rewards.length}`
  );

  //Guards
  if (isNaN(answer)) console.log("this is not a number");
  if (answer < 1 || answer > rewards.length || typeof answer !== "number") {
    console.log(`please try again`, typeof answer);
    playTheGame();
  }
  console.log(`your answer is ${answer}`);
  //find index
  index = rewards.findIndex((item) => {
    return item.number === answer;
  });
  console.log(`new index is `, index);
  console.log(rewards[index]);

  if(rewards[index]){

      //check the answer
      if (rewards[index].isReward) {
        alert("you win");
        body.style.background = "green";
      } else {
        yourTry++;
        rewards.splice(index, 1);
        console.log(`new rewards is `, rewards);
        alert(`oh no! you missed it and you have only ${maxTry - yourTry} chances`);
        playTheGame();
      }
  }else{
      playTheGame();
  }
};

playTheGame();
